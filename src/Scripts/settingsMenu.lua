local composer = require( "composer" )
local widget = require( "widget" )
local scene = composer.newScene()

--Constants
local _W, _H = display.actualContentWidth, display.actualContentHeight
local _DCX, _DCY = display.contentCenterX, display.contentCenterY

local AIon = false
local AIcomplexity = 1
local rowCount = 5
local colCount = 5
local winCount = 4
local playerCount = 2

local currentGridsSize
local currentPlayerCountBtn

local scrollView
local gridsGroup
    
local options =
{
    --required parameters
    width = 120,
    height = 64,
    numFrames = 2,

    --optional parameters; used for scaled content support
    sheetContentWidth = 240,  -- width of original 1x size of entire sheet
    sheetContentHeight = 64  -- height of original 1x size of entire sheet
}

local radioButtonSheet2 = graphics.newImageSheet( "Resources/Images/radioBtnSheet2.png", options )
local radioButtonSheet3 = graphics.newImageSheet( "Resources/Images/radioBtnSheet3.png", options )
local radioButtonSheet4 = graphics.newImageSheet( "Resources/Images/radioBtnSheet4.png", options )


local function animation( self )
   for i=1,gridsGroup.numChildren do
      transition.to( gridsGroup[i], {
      xScale = 1,
      yScale = 1,
      time = 50,
      } )
   end
   
   if (self ~= nil) then
      transition.to( self, {
      xScale = 1.3,
      yScale = 1.3,
      time = 100,
      } )
   end
   
end

function scrollListener( event )
   local phase = event.phase

   if (phase == "ended") then

      scrollView:scrollToPosition{
         x = -event.target.x+_W/2,
         time = 500,
      }

      if (true) then
         local i = 1
         for v in string.gmatch( event.target.id, "%d+" ) do
            if (i == 1) then
               rowCount = tonumber( v ) 
            elseif (i == 2) then
               colCount = tonumber( v ) 
            elseif (i == 3) then
               winCount = tonumber( v ) 
            end
            i = i + 1
         end
      end

      animation(event.target)   
   elseif ( phase == "moved" ) then
      local dx = math.abs( ( event.x - event.xStart ) )

      if ( dx > 5 ) then
         scrollView:takeFocus( event )
      end
   end
   return true
end

local function onSwitchRelease( event )
    if (AIon) then
        if (event.target.id == "rb2") then
          AIcomplexity = 1
        elseif (event.target.id == "rb3") then
          AIcomplexity = 2
        elseif (event.target.id == "rb4") then
          AIcomplexity = 3
        end
    elseif (not AIon) then
        if (event.target.id == "rb2") then
          playerCount = 2
        elseif (event.target.id == "rb3") then
          playerCount = 3
        elseif (event.target.id == "rb4") then
          playerCount = 4
        end
    end
end
      
local options = {
           style = "radio",
           width = 120,
           height = 64,
           onRelease = onSwitchRelease,
           frameOff = 1,
           frameOn = 2
       }

local transitionOptions = {
   effect = "slideRight",
   time = 200,
}



local function handleButtonEvent (event)
   if (event.target.id == "back") then
      composer.gotoScene( "Scripts.mainMenu", transitionOptions)
   elseif (event.target.id == "play") then
      local transitionOptionsParams = {
         effect = "slideLeft",
         time = 200,
         params = {
            rowCount = rowCount,
            colCount = colCount,
            winCount = winCount,
            playerCount = playerCount,
            AIon = AIon,
         }
      }
      composer.removeScene( "Scripts.gameScene" )
      composer.gotoScene( "Scripts.gameScene", transitionOptionsParams)
   end
end


local function createGridScroller(  )
   scrollView = widget.newScrollView(
        {
            left = 0,
            width = _W,
            height = 265,
            verticalScrollDisabled = true,
            backgroundColor = { 0,0,0,0 }
        }
    )
   
   gridsGroup = display.newGroup( )

   local optionsGridSelect = {
      width = 160,
      height = 190,
      defaultFile = "Resources/Images/gridSetting.png",
      overFile = "Resources/Images/gridSetting.png",
      onEvent = scrollListener
   }

   local grid1 = widget.newButton( optionsGridSelect )
   gridsGroup:insert( grid1 )
   grid1.id = "3.3.3"
   local grid2 = widget.newButton( optionsGridSelect )
   gridsGroup:insert( grid2 )
   grid2.id = "5.5.4"
   local grid3 = widget.newButton( optionsGridSelect )
   gridsGroup:insert( grid3 )
   grid3.id = "8.6.4"
   local grid4 = widget.newButton( optionsGridSelect )
   gridsGroup:insert( grid4 )
   grid4.id = "12.8.4"
   local grid5 = widget.newButton( optionsGridSelect )
   gridsGroup:insert( grid5 )
   grid5.id = "10.10.5"

   for i=1,gridsGroup.numChildren do
      gridsGroup[i].x = _W/2 + (i-1)*250
      gridsGroup[i].y = scrollView.contentHeight/2
   end

   scrollView:setScrollWidth( gridsGroup[gridsGroup.numChildren].x   + _W/2)
   scrollView:insert( gridsGroup )
   scrollView:scrollToPosition
   {
       x = -500,
       time = 100,
   }
   grid3.xScale = 1.3
   grid3.yScale = 1.3

end

function scene:create( event )
   local sceneGroup = self.view



   --CHOOSE GRIDS SIZE
   local gridSelectText = display.newText( sceneGroup, "Размер сетки", _DCX,60, "Calibri",20 )

   local backGridSelect = display.newImageRect( sceneGroup, "Resources/Images/backGridSelect.png", 730,355)
   backGridSelect.x = _DCX
   backGridSelect.y = 230

   createGridScroller()
   scrollView.x = _DCX
   scrollView.y = backGridSelect.y

   --CHOOSE PLAYERS AMOUNT
   local gridSelectText = display.newText( sceneGroup, "Количество игроков", _DCX,420, "Calibri",20 )
   if (AIon) then
       gridSelectText.text = "Сложность"
   end
   local backAmountPlayers = display.newImageRect( sceneGroup, "Resources/Images/backGridSelect.png", 730,100)
   backAmountPlayers.x = _DCX
   backAmountPlayers.y = 490

   local radioGroup = display.newGroup()
   sceneGroup:insert( radioGroup )

   options.sheet = radioButtonSheet2
   options.initialSwitchState = true
   local radioButton2 = widget.newSwitch( options )
   radioButton2.id = "rb2"
   radioGroup:insert( radioButton2 )
   radioButton2.x = _DCX - 150
   radioButton2.y = backAmountPlayers.y

   options.sheet = radioButtonSheet3
   options.initialSwitchState = false
   local radioButton3 = widget.newSwitch( options )
   radioButton3.sheet = radioButtonSheet3
   radioButton3.id = "rb3"
   radioGroup:insert( radioButton3 )
   radioButton3.x = _DCX
   radioButton3.y = backAmountPlayers.y

   options.sheet = radioButtonSheet4
   local radioButton4 = widget.newSwitch( options )
   radioButton4.sheet = radioButtonSheet4
   radioButton4.id = "rb4"
   radioGroup:insert( radioButton4 )
   radioButton4.x = _DCX + 150
   radioButton4.y = backAmountPlayers.y

   --Create Button
   local backBtn = widget.newButton( {
      width = 90,
     height = 90,
     defaultFile = "Resources/Images/backBtn.png",
     overFile = "Resources/Images/backBtn.png",
     id = "back",
     onRelease = handleButtonEvent
      } )
   backBtn.x = _DCX - 100
   backBtn.y = 720
   sceneGroup:insert( backBtn )

   local playBtn = widget.newButton( {
      width = 90,
     height = 90,
     defaultFile = "Resources/Images/playBtn.png",
     overFile = "Resources/Images/playBtn.png",
     id = "play",
     onRelease = handleButtonEvent
      } )
   playBtn.x = _DCX + 100
   playBtn.y = 720
   sceneGroup:insert( playBtn )


   sceneGroup:insert( scrollView )

end

function  scene:show( event )
     
end

function scene:show( event )

   local sceneGroup = self.view
   local phase = event.phase

    if ( phase == "will" ) then
      AIon = event.params.AIon
    elseif ( phase == "did" ) then
    end
end


---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )


---------------------------------------------------------------------------------

return scene
