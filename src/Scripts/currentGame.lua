local widget = require( "widget" )
local composer = require( "composer" )
local self


local function newGrid( row,col )
	local _Grid = display.newGroup( )
	local cellSize
	local offset = 8

	--Define and set size of one cell
	cellSize = ((display.actualContentWidth-100) / col) - offset
	
	if ((cellSize * row) + (offset * (col + 1)) > display.actualContentHeight  - 500) then
		cellSize = ((display.actualContentHeight  - 500) / row) - offset
	end

	--Create cells
	for h = 1,row do
		for w = 1,col do
			_Grid:insert(
					widget.newButton(
					    {	
					    	shape = "Rect",
					    	width = cellSize,
					    	height = cellSize,
					        id = tostring( w .. "." .. h ),
					        onEvent = gridsListener,
					        fillColor = { default={0.98,0.99,1}, over={0.98,0.99,1} }
					    }
					)
				)
			_Grid[_Grid.numChildren]:translate( (w * (cellSize+offset)) - cellSize, (h * (cellSize + offset)) - cellSize )
			_Grid[_Grid.numChildren].anchorX = 0
			_Grid[_Grid.numChildren].anchorY = 0
		end
	end

	_Grid:translate( display.contentCenterX, display.contentCenterY+150 )
	_Grid.anchorChildren = true

	--Create lines between cells
	local backOfGrid = display.newRect(cellSize*0.5,cellSize*0.5,_Grid.width + 2*offset,_Grid.height+ 2*offset)
	backOfGrid.anchorX=0
	backOfGrid.anchorY=0
	backOfGrid:setFillColor( 0.55,0.55,0.55,0.7 )
	_Grid:insert(backOfGrid)
	backOfGrid:toBack( )

	return _Grid
end

local function createTable( row,col )
	local _temp = {}
	for r=1,row do
		_temp[r] = {}
		for c=1,col do
			_temp[r][c] = 0
		end
	end
	return _temp
end

local function createTableScore( numPlayer )
	local table = {}
	for i=1,numPlayer do
		table[i] = 0 
	end
	return table
end



function newGame( interface, row, col, numPlayer, lengthToWin )
	local row = row or 5
	local col = col or 5
	local lengthToWin = lengthToWin or 3
	local currentPlayer = 1

	self = {}
	self.numPlayer = numPlayer or 2
	self.grid = display.newGroup( )
	self.moved = createTable(row,col)
	self.tableScore = createTableScore(self.numPlayer)

	local shapes = {
		"Resources/Images/circle.png",
		"Resources/Images/cross.png",
		"Resources/Images/triangle.png",
		"Resources/Images/zed.png",
	}

	local function roundIsFinish( playerWin )
		if (playerWin ~= 0) then
			self.tableScore[playerWin] = self.tableScore[playerWin] + 1
		end

		for i=2, row*col+1 do
			self.grid[i]:setEnabled( false )
		end


		print( "Score Player " .. 1 .. ": " .. self.tableScore[1] )
		print( "Score Player " .. 2 .. ": " .. self.tableScore[2] )

		composer.gotoScene( "Scripts.gameMenu", {
					effect = "slideRight",
		   			time = 200,
		   			params = {
		   				roundIsFinish = true,
		   				tableScore = self.tableScore
		   			}
		   			})
		print( "Player " .. playerWin .. " WIN !" )
	end

	function nextPlayer()
		if (currentPlayer == self.numPlayer) then
			currentPlayer = 1
		else
			currentPlayer = currentPlayer + 1
		end
	end

	function checkGame( )
		local currentLength = 0

		-- --Check Horizontal winer line
		for kR, vR in pairs(self.moved) do
			currentLength = 0
			for kC,vC in pairs(vR) do

				if (vC == currentPlayer) then
					currentLength = currentLength + 1
				else
					currentLength = 0	
				end

				if (currentLength == lengthToWin) then
					gameCanContinue = true
					roundIsFinish(currentPlayer)
					return true
				end
			end
		end

		currentLength = 0

		--Check Vertical winer line
		for i=1,table.getn(self.moved[1]) do
			currentLength = 0
			for kC,vC in pairs(self.moved) do

				if (vC[i] == currentPlayer) then
					currentLength = currentLength + 1
				else
					currentLength = 0	
				end

				if (currentLength == lengthToWin) then
					gameCanContinue = true
					roundIsFinish(currentPlayer)
					return true
				end
			end
		end

		currentLength = 0

		--Check Diagonal Right winer line
		for kR, vR in pairs(self.moved) do
			for kC,vC in pairs(vR) do
				if (vC == currentPlayer) then
					currentLength = currentLength + 1

					for i=1,lengthToWin do

						if ((i+kC)>table.getn(vR) or (i+kR)>table.getn(self.moved)) then
							currentLength = 0
							break
						end

						if (self.moved[kR+i][kC+i] == currentPlayer) then
							currentLength = currentLength + 1
							
						else
							currentLength = 0
							break	
						end

						if (currentLength == lengthToWin) then
							gameCanContinue = true
							roundIsFinish(currentPlayer)
							return true
						end
					end
				else
					currentLength = 0
				end
			end
		end

		currentLength = 0

		--Check Diagonal Left winner line
		for kR, vR in pairs(self.moved) do
			for kC,vC in pairs(vR) do
				if (vC == currentPlayer) then
					currentLength = currentLength + 1

					for i=1,lengthToWin do

						if ((kC-i)<1 or (kR+i)>table.getn(self.moved)) then
							currentLength = 0
							break
						end

						if (self.moved[kR+i][kC-i] == currentPlayer) then
							currentLength = currentLength + 1
							
						else
							currentLength = 0
							break	
						end

						if (currentLength == lengthToWin) then
							gameCanContinue = true
							roundIsFinish(currentPlayer)
							return true
						end
					end
				else
					currentLength = 0
				end
			end
		end

		currentLength = 0
		--Check Draw
		local gameCanContinue = false
		for kR, vR in pairs(self.moved) do
			for kC,vC in pairs(vR) do
				if(vC == 0) then
					gameCanContinue = true
				end
			end
		end
		if (gameCanContinue == false) then 
			roundIsFinish(0)
			gameCanContinue = true 
			return true
		end

		nextPlayer()
	end

	

	function gridsListener( event )
		if("ended" == event.phase ) then
			local cell = event.target

			local x, y
			for i in string.gmatch( cell.id, "%d+" ) do
				if x == nil then x = tonumber(i) else y = tonumber(i) end
			end

			if (self.moved[y][x] == 0) then
				self.moved[y][x] = currentPlayer

				local posX,posY = cell.x,cell.y
				local shape = display.newImage( shapes[currentPlayer],posX + cell.width/2,posY + cell.height/2)
				shape.width = cell.width*0.8
				shape.height = cell.width*0.8
				self.grid:insert( shape )

				event.target:setEnabled( false )
			end

			checkGame()
			
			
			print( "Cell " .. y .. " " .. x .. " was pressed" )
		end
	end

	function self.restartRound(  )
		self.grid:removeSelf( )
		self.grid = nil
		self.moved = nil
		self.grid = newGrid(row,col)
		self.moved = createTable(row,col)
	end
	
	self.grid=newGrid(row,col)
	return self

end