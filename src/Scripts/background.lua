local _W, _H = display.contentWidth, display.contentHeight
local _DCX, _DCY = display.contentCenterX, display.contentCenterY


local convertRGB = function(r, g, b)
   assert(r and g and b and r <= 255 and r >= 0 and g <= 255 and g >= 0 and b <= 255 and b >= 0, "You must pass all 3 RGB values within a range of 0-255");
   return r/255, g/255, b/255;
end

function newBackground(  )
	local self = display.newGroup( )

	local back = display.newRect( self, _DCX,_DCY, 800,800)

	back.fill.effect = "generator.radialGradient"

	back.fill.effect.color1 = { convertRGB(50,57,69) }
	back.fill.effect.color2 = { 0.1, 0.1, 0.1,  }
	back.fill.effect.center_and_radiuses  =  { 0.5, 0.5, 0.6, 0.99 }
	back.fill.effect.aspectRatio  = 1
	
    return self
end