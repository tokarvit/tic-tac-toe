local ScoreWin = 2
local ScoreLose = -2
local ScoreDraw = 1
local ScoreNone = 0

local AIplayer
local UserPlayer
local board
local computersMove = {}
local gameLengthToWin
local initialDepth

local function checkGame(moved,currentPlayer,lengthToWin)
	--print( "CHECK GAME" )
	
	local currentLength = 0


	-- --Check Horizontal winer line
	for kR, vR in pairs(moved) do
		local firstSideClosed = true
		local secondSideClosed = true
		currentLength = 0
		for kC,vC in pairs(vR) do

			if (vC == currentPlayer) then
				-- if (currentLength == 0 and (vR[kC-1] == nil or vR[kC-1] ~= currentPlayer)) then
				-- 	print( "First side is close" )
				-- 	firstSideClosed = true
				-- end
				currentLength = currentLength + 1
			else
				currentLength = 0	
			end

			if (currentLength == lengthToWin) then
				if (vR[kC+1] == 0) then
					secondSideClosed = false
				end
				if (vR[kC-lengthToWin] == 0) then
					firstSideClosed = false
				end

				if (secondSideClosed and firstSideClosed) then
					--print( "secondSideClosed: " .. tostring( secondSideClosed )  .. " " .. "firstSideClosed: " .. tostring( firstSideClosed )  )
					return "winWithBothSidesClosed"
				elseif (firstSideClosed) then
					return "winWithFirstSideClosed"
				elseif (secondSideClosed) then
					return "winWithSecondSideClosed"
				end

				return "winWithoutClosed"
			end
		end
	end

	currentLength = 0

	--Check Vertical winer line
	for i=1,table.getn(moved[1]) do
		local firstSideClosed = true
		local secondSideClosed = true
		currentLength = 0
		for kC,vC in pairs(moved) do

			if (vC[i] == currentPlayer) then
				currentLength = currentLength + 1
			else
				currentLength = 0	
			end

			if (currentLength == lengthToWin) then
				if (moved[kC+1] and moved[kC+1][i] == 0) then
					--print( "moved[kC+1][i] " .. (kC+1) .. ":" .. i )
					secondSideClosed = false
				end
				if (moved[kC-lengthToWin] and moved[kC-lengthToWin][i] == 0) then
					--print( "moved[kC-lengthToWin][i] " .. (kC-lengthToWin) .. ":" .. i )
					firstSideClosed = false
				end

				if (secondSideClosed and firstSideClosed) then
					--print( "secondSideClosed: " .. tostring( secondSideClosed )  .. " " .. "firstSideClosed: " .. tostring( firstSideClosed )  )
					firstSideClosed = true
					secondSideClosed = true
					return "winWithBothSidesClosed"
				elseif (firstSideClosed) then
					firstSideClosed = true
					secondSideClosed = true
					return "winWithFirstSideClosed"
				elseif (secondSideClosed) then
					firstSideClosed = true
					secondSideClosed = true
					return "winWithSecondSideClosed"
				end

				return "winWithoutClosed"
			end
		end
	end

	currentLength = 0

	--Check Diagonal Right winer line
	for kR, vR in pairs(moved) do
		for kC,vC in pairs(vR) do
			if (vC == currentPlayer) then
				currentLength = currentLength + 1

				for i=1,lengthToWin do

					if ((i+kC)>table.getn(vR) or (i+kR)>table.getn(moved)) then
						currentLength = 0
						break
					end

					if (moved[kR+i][kC+i] == currentPlayer) then
						currentLength = currentLength + 1
						
					else
						currentLength = 0
						break	
					end

					if (currentLength == lengthToWin) then
						local firstSideClosed = true
						local secondSideClosed = true

						if (moved[kR+i+1] and moved[kR+i+1][kC+i+1] == 0) then
							secondSideClosed = false
						end
						if (moved[kR+i-lengthToWin] and moved[kR+i-lengthToWin][kC+i-lengthToWin] == 0) then
							firstSideClosed = false
						end

						if (secondSideClosed and firstSideClosed) then
							--print( "secondSideClosed: " .. tostring( secondSideClosed )  .. " " .. "firstSideClosed: " .. tostring( firstSideClosed )  )
							firstSideClosed = true
							secondSideClosed = true
							return "winWithBothSidesClosed"
						elseif (firstSideClosed) then
							firstSideClosed = true
							secondSideClosed = true
							return "winWithFirstSideClosed"
						elseif (secondSideClosed) then
							firstSideClosed = true
							secondSideClosed = true
							return "winWithSecondSideClosed"
						end
						
						return "winWithoutClosed"
					end
				end
			else
				currentLength = 0
			end
		end
	end

	currentLength = 0

	--Check Diagonal Left winner line
	for kR, vR in pairs(moved) do
		for kC,vC in pairs(vR) do
			if (vC == currentPlayer) then
				currentLength = currentLength + 1

				for i=1,lengthToWin do

					if ((kC-i)<1 or (kR+i)>table.getn(moved)) then
						currentLength = 0
						break
					end

					if (moved[kR+i][kC-i] == currentPlayer) then
						currentLength = currentLength + 1
						
					else
						currentLength = 0
						break	
					end

					if (currentLength == lengthToWin) then
						local firstSideClosed = true
						local secondSideClosed = true
						
						if (moved[kR+i+1] and moved[kR+i+1][kC-i-1] == 0) then
							secondSideClosed = false
						end
						if (moved[kR-1] and moved[kR-1][kC+1] == 0) then
							firstSideClosed = false
						end

						if (secondSideClosed and firstSideClosed) then
							--print( "secondSideClosed: " .. tostring( secondSideClosed )  .. " " .. "firstSideClosed: " .. tostring( firstSideClosed )  )
							firstSideClosed = true
							secondSideClosed = true
							return "winWithBothSidesClosed"
						elseif (firstSideClosed) then
							firstSideClosed = true
							secondSideClosed = true
							return "winWithFirstSideClosed"
						elseif (secondSideClosed) then
							firstSideClosed = true
							secondSideClosed = true
							return "winWithSecondSideClosed"
						end
						
						return "winWithoutClosed"
					end
				end
			else
				currentLength = 0
			end
		end
	end

	currentLength = 0
	--Check Draw
	local gameCanContinue = false
	for kR, vR in pairs(moved) do
		for kC,vC in pairs(vR) do
			if(vC == 0) then
				gameCanContinue = true
			end
		end
	end
	if (gameCanContinue == false) then  
		return "draw"
	end

	return "nothing"
end

local function getAllAvailableMove(b)
	local allAvailableMove = {}
	for y=1, table.maxn( b ) do
		for x=1,table.maxn( b[y] ) do
			if (b[y][x] == 0) then
				--print("              " .. y .. ":" .. x )
				table.insert( allAvailableMove, table.maxn( allAvailableMove )+1, {x,y} )
			end
		end
	end
	return allAvailableMove
end

function showTable(  )
	for y=1,table.maxn( board ) do
		for x=1,table.maxn( board[y] ) do
			io.write( board[y][x])
		end
		print("" )
	end
end

local alpha = 10
local beta = -10

local function miniMax( depth, currentPlayer )
	--showTable(  )

	local _tempAI = checkGame(board, AIplayer, gameLengthToWin)
	if (string.find( _tempAI, "win")) then
		return 10*(depth+1)
	elseif (_tempAI == "draw") then 
		return 0
	end

	local _tempUSER = checkGame(board, UserPlayer, gameLengthToWin)
	if (  string.find( _tempUSER, "win")) then
		return -9*(depth+1)
	end

	if (depth == (initialDepth - 2)) then
		local _enoughtToLose = checkGame(board, UserPlayer, gameLengthToWin - 1)
		--print( "Enought to lose = " .. _enoughtToLose )
		if (_enoughtToLose == "winWithoutClosed") then
			--showTable(  )
			print( "Enought to lose!" )
			return -5
		end
	end

	

	-- for i=gameLengthToWin-1, 2 , -1 do
	-- 	local _tempAI = checkGame(board, AIplayer, i)
	-- 	if (_tempAI==1 and depth ~= initialDepth) then
	-- 		return i*(depth+1)
	-- 	end

	-- 	local _tempUSER = checkGame(board, UserPlayer, i)
	-- 	if ( _tempUSER == 1 and depth ~= initialDepth) then
	-- 		return -(i-1)*(depth+1)
	-- 	elseif (_tempUSER == 0) then 
	-- 		return 0
	-- 	end
	-- end

	if (depth == 0) then
		return 0
	end

	local min , max = 1000, -1000

	local allAvailableMove = getAllAvailableMove(board)
	for i=1,table.maxn( allAvailableMove ) do
		local point = allAvailableMove[i]
		board[point[2]][point[1]] = currentPlayer
		if (currentPlayer == AIplayer) then
			local currentScore = miniMax (depth - 1, UserPlayer)

			--max = math.max( currentScore, max)
			if (currentScore > max) then
				max = currentScore
				if (depth == initialDepth) then
					computersMove = {}
					table.insert( computersMove, table.maxn( computersMove )+1, point )
				end
			end
			if (currentScore == max and depth == initialDepth) then
					table.insert( computersMove, table.maxn( computersMove )+1, point )
			end
			if (depth == initialDepth) then
				print( "Score for position "..point[2]..":" .. point[1].." = "..currentScore )
			end
			if (currentScore>=alpha*(depth+1)) then
				board[point[2]][point[1]] = 0
				break
			end
		elseif (currentPlayer == UserPlayer) then
			local currentScore = miniMax (depth - 1, AIplayer)
			min = math.min (currentScore, min)
			if (currentScore<=beta*(depth+1)) then
				board[point[2]][point[1]] = 0
				break
			end
		end
		board[point[2]][point[1]] = 0
	end





	if (currentPlayer == AIplayer) then
		--print( "Return MAX "..max )
		return max
	elseif (currentPlayer == UserPlayer) then
		--print( "Return MIN "..min )
		return min
	end
end

function AImove( b, depth, AI, winL)
	AIplayer = AI
	board = b
	gameLengthToWin = winL
	initialDepth = depth
	if (AIplayer == 1) then
		UserPlayer = 2
	else
		UserPlayer = 1
	end

	miniMax(depth, AIplayer)
	return unpack( computersMove[math.random(1,table.maxn(computersMove))] )
end

