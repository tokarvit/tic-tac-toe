local widget = require( "widget" )
local composer = require( "composer" )

local _W, _H = display.actualContentWidth, display.actualContentHeight
local _DCX, _DCY = display.contentCenterX, display.contentCenterY

local buttonPositionY = 150



function newInterface()
	local interface = display.newGroup( )
	  

	
    

    local panel = display.newImage( interface,"Resources/Images/topShape.png", _DCX,0)
	panel.anchorY = -1

	local currentPlayerText = display.newText( interface, " Player 1", panel.x,100, "Resources/komikaAxis.ttf" , 70 )
	
	function interface.changePlayerText( event )
		currentPlayerText.text = "" .. event.player
	end


	local menuBtn = widget.newButton( {
			defaultFile = "Resources/Images/menuBtn.png",
			overFile = "Resources/Images/menuBtnOver.png",
	        id = "menu",
	        onRelease = interfaceListener,
	        fillColor = { default={ 1,0,0 }, 
	                    over={ 1,0,0 } }
		} )
	menuBtn.x = 400
	menuBtn.y = buttonPositionY
	
	local soundBtn = widget.newButton( {
			defaultFile = "Resources/Images/soundBtn.png",
			overFile = "Resources/Images/soundBtnOver.png",
	        id = "sound",
	        onRelease = interfaceListener,
	        fillColor = { default={ 1,0,0 }, 
	                    over={ 1,0,0 } }
		} )
	soundBtn.x = display.contentWidth - 400
	soundBtn.y = buttonPositionY

	
	
	interface:insert( menuBtn )
	interface:insert( soundBtn )

	Runtime:addEventListener( "Change Player Name", interface.changePlayerText )
	
	function interface.deleteListener(  )
		Runtime:removeEventListener( "Change Player Name", interface.changePlayerText )
		print( "Listeners have been removed" )
	end

	return interface
end
