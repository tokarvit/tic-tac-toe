local composer = require( "composer" )
local scene = composer.newScene()
local widget = require( "widget" )
require( "Scripts.background" )
background = newBackground()
background:toBack( )

local logo
local continueBtn
local playBtn
local friendsBtn

local _W, _H = display.viewableContentWidth, display.viewableContentHeight
local _DCX, _DCY = display.contentCenterX, display.contentCenterY




local function btnAnimation( event )
    if (event.phase == "began") then
    	if (event.target.id == "continue") then
    		transition.cancel( "btn" )
        	composer.gotoScene( "Scripts.gameScene", transitionOptions)
    	end
   		
    	transition.to( event.target, {time = 100, xScale = 1.1, yScale = 1.1})
    elseif (event.phase == "ended") then
     	transition.to( event.target, {time = 100, xScale = 1, yScale = 1})

        if(event.target.id == "single") then
			local transitionOptions = {
				effect = "slideLeft",
				time = 200,
				params = {
					AIon = true,
				}
			}
          	composer.removeScene( "Scripts.settingsMenu" )
          	composer.gotoScene( "Scripts.settingsMenu", transitionOptions)
        elseif(event.target.id == "friends" ) then
	        local transitionOptions = {
					effect = "slideLeft",
					time = 200,
					params = {
						AIon = false,
					}
			}
          	composer.removeScene( "Scripts.settingsMenu" )
        	composer.gotoScene( "Scripts.settingsMenu", transitionOptions)
        elseif(event.target.id == "continue" ) then
          	transition.cancel( "btn" )
          	composer.gotoScene( "Scripts.gameScene", transitionOptions)
        end

    elseif (event.phase == "cancelled") then
    	transition.to( event.target, {time = 100, xScale = 1, yScale = 1})

    	if (event.target.id == "continue") then
    		continueBtn:anim()
    	end
    end
end

local options = {
   defaultFile = "Resources/Images/btn.png",
   width = 0.7 * _W,
   height = (0.7 * _W)/4,
   font = "Resources/komikaAxis.ttf",
   fontSize = 28,
   labelYOffset = -8,
   onEvent = btnAnimation,
   labelColor = { default={ 1, 1, 1 }, over={  1, 1, 1 } },
   fillColor = { default={ 0.929, 0.322, 0.315}, over={  0.929, 0.322, 0.315} }

}

local function newBtn( text, id )
   local btn = widget.newButton( options )
   btn:setLabel( text )
   btn.id = id

   function btn:anim(  )

      local function animFirst()
      end

      local function animSecond( ... )
         transition.to( self, {time = 1000, xScale = 1, yScale = 1, onComplete = animFirst, tag = "btn", 
            onCancel = function ( ... )
         end} )
      end

      function animFirst( ... )
         anim = transition.to( self, {time = 1000, xScale = 1.1, yScale = 1.1, onComplete = animSecond, tag = "btn", 
            onCancel = function ( ... )
         end} )
      end
      
      animFirst()
   end

   return btn
end

function scene:create( event )

   local sceneGroup = self.view
   

   logo = display.newImageRect( sceneGroup, "Resources/Images/logo.png", 350,240 )
   logo:translate( display.contentCenterX, display.viewableContentHeight * 0.25)

   continueBtn = newBtn(" Продолжить", "continue")
   sceneGroup:insert( continueBtn , true)
   continueBtn:translate( _DCX, logo.y+logo.height )
   continueBtn.alpha = 0

   playBtn = newBtn(" Играть", "single")
   sceneGroup:insert( playBtn , true)
   playBtn:translate( _DCX, 550 )

   friendsBtn = newBtn(" Играть с друзьями", "friends")
   friendsBtn._view._label.size = 22
   sceneGroup:insert( friendsBtn , true)
   friendsBtn:translate( _DCX, playBtn.y + friendsBtn.height  )
    
end

-- "scene:show()"
function scene:show( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then
      if (event.params) then
        print( "Continue btn are required" )
        continueBtn.alpha = 1
        continueBtn:anim()
      end
   elseif ( phase == "did" ) then
      --continueBtn:anim()
   end
end

-- "scene:hide()"
function scene:hide( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then
      transition.cancel( "btn" )
   elseif ( phase == "did" ) then
      
   end
end

-- "scene:destroy()"
function scene:destroy( event )

   local sceneGroup = self.view

   -- Called prior to the removal of scene's view ("sceneGroup").
   -- Insert code here to clean up the scene.
   -- Example: remove display objects, save state, etc.
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene
