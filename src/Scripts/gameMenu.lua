local composer = require( "composer" )
local widget = require( "widget" )
local scene = composer.newScene()

local delayTime = 3000
local dTimer
local transitionOptions = {
   
}

local container

local function btnListener( event )
   dTimer = 0
   if(event.target.id == "resume") then
      composer.gotoScene( "Scripts.gameScene", {
         effect = "slideRight",
         time = 200})
   elseif(event.target.id == "restartRound" ) then
      composer.gotoScene( "Scripts.gameScene", {
         effect = "slideRight",
         time = 200,
         params = {
            restartRound = "true",
         }
         })
   elseif(event.target.id == "newGame" ) then
      composer.gotoScene( "Scripts.settingsMenu", {
         effect = "slideRight",
         time = 200})
   end

   return true
end

local options = {
   defaultFile = "Resources/Images/button.png",
   font = "Resources/komikaAxis.ttf",
   fontSize = 80,
   onRelease = btnListener,
   labelColor = { default={ 1, 1, 1 }, over={  1, 1, 1 } }
}

local function setPos( self )
   self.x = display.contentCenterX
   self.rotation = -6
   self.width = display.viewableContentWidth*0.85
end

local function showTable( numPlayer )
   local indent = 50
   local containerTemp = display.newGroup( )

   for i=1,numPlayer do
      local containerRow = display.newGroup(  )

      local playerName = display.newText( containerRow, "Player " .. i, display.contentCenterX-display.viewableContentWidth*0.70/2-190,i*200+300, "Resources/komikaAxis.ttf", 60 )
      playerName.anchorX = 0
      playerName:setTextColor( .24,.32,.4 )

      local scoreText = display.newText( containerRow, 0, display.contentCenterX+display.viewableContentWidth*0.65/2,i*200+300, "Resources/komikaAxis.ttf", 60 )
      scoreText.anchorX = 1
      scoreText:setTextColor( .24,.32,.4 )

      containerRow.rotation = -6
      containerTemp:insert( containerRow )
   end

   
   

   return containerTemp
end

function scene:create( event )

   local sceneGroup = self.view

   if (event.params.numPlayer) then
      container = showTable( event.params.numPlayer )
   elseif (event.params.tableScore) then
      container = showTable( table.maxn( event.params.tableScore ) )
   end
   

   local restartRoundBtn = widget.newButton( options )
   restartRoundBtn.y = display.contentCenterY + 150
   restartRoundBtn:setLabel( "Restart Round" )
   restartRoundBtn.id = "restartRound"
   setPos(restartRoundBtn)

   local newGameBtn = widget.newButton( options )
   newGameBtn.y = display.contentCenterY + 550
   newGameBtn:setLabel( "New Game" )
   newGameBtn.id = "newGame"
   setPos(newGameBtn)

   local resumeBtn= widget.newButton( {
      defaultFile = "Resources/Images/backBtn.png",
      overFile = "Resources/Images/backBtnOver.png",
      onRelease = btnListener,
      } )
   resumeBtn.x = display.contentCenterX
   resumeBtn.y = display.contentCenterY + 1100
   resumeBtn.id = "resume"

   sceneGroup:insert( container )
   sceneGroup:insert( restartRoundBtn )
   sceneGroup:insert( newGameBtn )
   sceneGroup:insert( resumeBtn )


end

-- "scene:show()"
function scene:show( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then
      if (event.params and event.params.roundIsFinish) then
         for i=1,table.maxn( event.params.tableScore )  do
            container[i][2].text = event.params.tableScore[i]
         end

         dTimer = system.getTimer( )

      end
   elseif ( phase == "did" ) then
      -- Called when the scene is now on screen.
      -- Insert code here to make the scene come alive.
      -- Example: start timers, begin animation, play audio, etc.
   end
end

-- "scene:hide()"
function scene:hide( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then
      -- Called when the scene is on screen (but is about to go off screen).
      -- Insert code here to "pause" the scene.
      -- Example: stop timers, stop animation, stop audio, etc.
   elseif ( phase == "did" ) then
      -- Called immediately after scene goes off screen.
   end
end

-- "scene:destroy()"
function scene:destroy( event )

   local sceneGroup = self.view

   -- Called prior to the removal of scene's view ("sceneGroup").
   -- Insert code here to clean up the scene.
   -- Example: remove display objects, save state, etc.
end

function gameLoop( event )
   if (dTimer and dTimer>0) then
      local t =system.getTimer( )
      if ((t-dTimer)>delayTime) then
          composer.gotoScene( "Scripts.gameScene", {
         effect = "slideRight",
         time = 200,
         params = {
            restartRound = "true",
         }
         })
          dTimer = 0
      end

   end
end
---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
Runtime:addEventListener( "enterFrame", gameLoop )

---------------------------------------------------------------------------------

return scene
