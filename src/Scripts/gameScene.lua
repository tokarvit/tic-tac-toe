local composer = require( "composer" )
local widget = require( "widget" )
local scene = composer.newScene(  )

require( "Scripts.interface" )
require( "Scripts.background" )
require( "Scripts.currentGame" )
require( "Scripts.AI" )

--Constants
local _W, _H = display.actualContentWidth, display.actualContentHeight
local _DCX, _DCY = display.contentCenterX, display.contentCenterY

--Variables 
local row 
local col
local lengthToWin 
local currentPlayer 
local numPlayer 
local grid 
local moved
local tableScore
local AIon
local shapes = {
		"Resources/Images/circle.png",
		"Resources/Images/cross.png",
		"Resources/Images/triangle.png",
		"Resources/Images/zed.png",
	}
local interface
local endMenuGroup = display.newGroup( )
local transitionOptions = {
   effect = "slideRight",
   time = 200,
   params = {
   		showContinueBtn = true
   }
}
local checkGame

local function showEndMenu( onoff )
	local animTime = 100

	if (onoff == true) then
	    transition.to( endMenuGroup[1], {time = animTime, alpha=1})
		transition.to( endMenuGroup[2], {time = animTime, y=600})
		transition.to( endMenuGroup[3], {delay = 40, time = animTime, y=700})
		transition.to( endMenuGroup[4], {delay = 40, time = animTime, y=700})
	elseif (onoff == false) then
		transition.to( endMenuGroup[4], {time = animTime, y=900})
		transition.to( endMenuGroup[3], {time = animTime, y=900})
		transition.to( endMenuGroup[2], {delay = 40, time = animTime, y=900})
		transition.to( endMenuGroup[1], {time = animTime, alpha=0})
	end
end

local function showEndMenuListener( event )
	if (event.target.id == "restart") then
		newRound()
		showEndMenu(false)
	elseif (event.target.id == "return") then
		showEndMenu(false)
	elseif (event.target.id == "home") then
		composer.gotoScene( "Scripts.mainMenu", transitionOptions)
		showEndMenu(false)
	end
end

local function initEndMenu(  )
    local blackLayer = widget.newButton( {
    		shape = "rect",
			width = _W,
			height = _H,
			id = "return",
			fillColor = { default={ 0,0,0,0.5 }, over={ 0,0,0,0.5 } },
			onRelease = showEndMenuListener
		} )
	blackLayer.x = _DCX
	blackLayer.y = _DCY
	blackLayer.alpha = 0
	endMenuGroup:insert( blackLayer )

	local restartBtn = widget.newButton( {
			defaultFile = "Resources/Images/restartBtn.png",
			width = btnSize,
			height = btnSize,
			id = "restart",
			onRelease = showEndMenuListener
		} )
	restartBtn.x = _DCX
	restartBtn.y = 900
	endMenuGroup:insert( restartBtn )

	local homeBtn = widget.newButton( {
			defaultFile = "Resources/Images/homeBtn.png",
			width = btnSize,
			height = btnSize,
			id = "home",
			onRelease = showEndMenuListener
		} )
	homeBtn.x = _DCX - 100
	homeBtn.y = 900
	endMenuGroup:insert( homeBtn )

	local soundBtn = widget.newButton( {
			defaultFile = "Resources/Images/soundBtn.png",
			width = btnSize,
			height = btnSize,
			id = "sound",
			onRelease = showEndMenuListener
		} )
	soundBtn.x = _DCX + 100
	soundBtn.y = 900
	endMenuGroup:insert( soundBtn )
end

local function nextPlayer()
	if (currentPlayer == numPlayer) then
		currentPlayer = 1
	else
		currentPlayer = currentPlayer + 1
	end

	currentPlayerView()

	if (AIon and currentPlayer == 2) then
		-- for i=1, row*col do
		-- 	grid[i]:setEnabled( false )
		-- end
		local x,y = AImove(moved, 2,currentPlayer, lengthToWin)

		print( "AI moved: " .. y .. ":" .. x )
		moved[y][x] = currentPlayer
		local cell = grid[(y-1)*row + x]

		local posX,posY = cell.x,cell.y
		local shape = display.newImage( shapes[currentPlayer],posX + cell.width/2,posY + cell.height/2)
		shape.width = cell.width*0.8
		shape.height = cell.width*0.8
		grid:insert( shape )
		grid[(y-1)*row + x]:setEnabled( false )


		-- for y=1, table.maxn( moved ) do
		-- 	for x=1,table.maxn( moved[y] ) do
		-- 		if (moved[y][x] == 0) then
		-- 			grid[(y-1)*row+x]:setEnabled( true )
		-- 		end
		-- 	end
		-- end
		checkGame()

	end

	
end

local function roundIsFinish( playerWin )
	if (playerWin ~= 0) then
		tableScore[playerWin] = tableScore[playerWin] + 1
	end

	setScoreText()

	for i=1, row*col do
		grid[i]:setEnabled( false )
	end

	showEndMenu(true)
	print( "Player " .. playerWin .. " WIN !" )
end

checkGame = function ()
	local _time = system.getTimer()

	for y=1,table.maxn( moved ) do
		for x=1,table.maxn( moved[y] ) do
			io.write( moved[y][x])
		end
		print("" )
	end
	local currentLength = 0

	-- --Check Horizontal winer line
	for kR, vR in pairs(moved) do
		currentLength = 0
		for kC,vC in pairs(vR) do

			if (vC == currentPlayer) then
				currentLength = currentLength + 1
			else
				currentLength = 0	
			end
				--print( "Current length " .. currentLength .. " of " .. lengthToWin .. " player " .. currentPlayer .. " : "..kR..":"..kC)

			if (currentLength == lengthToWin) then
				print( "WIIIIIIIIIN" )
				gameCanContinue = true
				roundIsFinish(currentPlayer)
				return true
			end
		end
	end

	currentLength = 0

	--Check Vertical winer line
	for i=1,table.getn(moved[1]) do
		currentLength = 0
		for kC,vC in pairs(moved) do

			if (vC[i] == currentPlayer) then
				currentLength = currentLength + 1
			else
				currentLength = 0	
			end

			if (currentLength == lengthToWin) then
				gameCanContinue = true
				roundIsFinish(currentPlayer)
				return true
			end
		end
	end

	currentLength = 0

	--Check Diagonal Right winer line
	for kR, vR in pairs(moved) do
		for kC,vC in pairs(vR) do
			if (vC == currentPlayer) then
				currentLength = currentLength + 1

				for i=1,lengthToWin do

					if ((i+kC)>table.getn(vR) or (i+kR)>table.getn(moved)) then
						currentLength = 0
						break
					end

					if (moved[kR+i][kC+i] == currentPlayer) then
						currentLength = currentLength + 1
						
					else
						currentLength = 0
						break	
					end

					if (currentLength == lengthToWin) then
						gameCanContinue = true
						roundIsFinish(currentPlayer)
						return true
					end
				end
			else
				currentLength = 0
			end
		end
	end

	currentLength = 0

	--Check Diagonal Left winner line
	for kR, vR in pairs(moved) do
		for kC,vC in pairs(vR) do
			if (vC == currentPlayer) then
				currentLength = currentLength + 1

				for i=1,lengthToWin do

					if ((kC-i)<1 or (kR+i)>table.getn(moved)) then
						currentLength = 0
						break
					end

					if (moved[kR+i][kC-i] == currentPlayer) then
						currentLength = currentLength + 1
						
					else
						currentLength = 0
						break	
					end

					if (currentLength == lengthToWin) then
						gameCanContinue = true
						roundIsFinish(currentPlayer)
						return true
					end
				end
			else
				currentLength = 0
			end
		end
	end

	currentLength = 0
	--Check Draw
	local gameCanContinue = false
	for kR, vR in pairs(moved) do
		for kC,vC in pairs(vR) do
			if(vC == 0) then
				gameCanContinue = true
			end
		end
	end
	if (gameCanContinue == false) then 
		roundIsFinish(0)
		gameCanContinue = true 
		return true
	end

	print( "nextPlayer" )
	nextPlayer()
	print( "Spended time for Check Game function " .. system.getTimer()-_time )
end

local function interfaceListener( event )
	if (event.target.id == "menu") then
		showEndMenu(true)
		print("Menu button")
	elseif (event.target.id == "sound") then
		
	elseif (event.target.id == "restart") then
		newRound()
	end
end

local function gridsListener( event )
	if("ended" == event.phase ) then
		local cell = event.target

		local x, y
		for i in string.gmatch( cell.id, "%d+" ) do
			if x == nil then x = tonumber(i) else y = tonumber(i) end
		end

		if (moved[y][x] == 0) then
			moved[y][x] = currentPlayer

			local posX,posY = cell.x,cell.y
			local shape = display.newImage( shapes[currentPlayer],posX + cell.width/2,posY + cell.height/2)
			shape.width = cell.width*0.8
			shape.height = cell.width*0.8
			grid:insert( shape )
			event.target:setEnabled( false )
		end

		checkGame()
		
		
		print( "Cell " .. y .. " " .. x .. " was pressed" )
	end
end

local function initInterface()
	local btnSize = 80
	local interfaceGroup = display.newGroup( )
	local interfacePanel = display.newImageRect( interfaceGroup, "Resources/Images/ribbon.png", 300,805 )
	interfacePanel.anchorY = 0
	interfacePanel.x = _DCX
	interfacePanel.y = 0

	-- local interfacePanelStroke = display.newRect( interfaceGroup, _DCX,-10, _W*0.48,250 )
	-- interfacePanelStroke.anchorY = 0
	-- interfacePanelStroke:setFillColor( 0,0,0,0 )
	-- interfacePanelStroke:setStrokeColor( 1,1,1 )
	-- interfacePanelStroke.strokeWidth = 2

	local currentPlayerRect = display.newRoundedRect( interfaceGroup, _DCX,50, interfacePanel.width*0.78, 30,10)

	local menuBtn = widget.newButton( {
			defaultFile = "Resources/Images/menuBtn.png",
			width = btnSize,
			height = btnSize,
			id = "menu",
			onRelease = interfaceListener
		} )
	menuBtn.anchorX = 1
	menuBtn:translate( display.screenOriginX + (_DCX-display.screenOriginX - interfacePanel.width/2)/2, 20 )

	local restartBtn = widget.newButton( {
			defaultFile = "Resources/Images/restartBtn.png",
			width = btnSize,
			height = btnSize,
			id = "restart",
			onRelease = interfaceListener
		} )
	restartBtn.anchorX = 1
	restartBtn:translate( 730 - (display.screenOriginX + (_DCX-display.screenOriginX - interfacePanel.width/2)/2), 20 )

	

	local tableWinners = display.newGroup(  )
	textY = 150/numPlayer 
	for i=1,numPlayer do
		local playerName = display.newText( tableWinners, "Players "..i,
			_DCX-interfacePanel.width/2+40, i*textY/2+10*(i-1), "Resources/komikaAxis.ttf" , 20 )
		playerName:setTextColor( 1 )
		playerName.anchorX = 0
		
		local score = display.newText( tableWinners, 0,
			_DCX+interfacePanel.width/2-40, i*textY/2+10*(i-1), "Resources/komikaAxis.ttf" , 20 )
		score.anchorX = 1
	end
	function setScoreText()
		for i=1,numPlayer do
			tableWinners[i*2].text = tableScore[i]
		end
	end
	function currentPlayerView(  )
		transition.to( currentPlayerRect, {time = 100,y = tableWinners[currentPlayer*2].y+2} )
		for i=1,tableWinners.numChildren do
			tableWinners[i]:setTextColor( 1,1,1 )
		end
		tableWinners[currentPlayer*2-1]:setTextColor( 0.929, 0.322, 0.315 )
		tableWinners[currentPlayer*2]:setTextColor( 0.929, 0.322, 0.315 )
	end
	interfaceGroup:insert( tableWinners )

	
	interfaceGroup:insert( menuBtn )
	interfaceGroup:insert( restartBtn )

	return interfaceGroup
end

local function initGrid( row,col )
	local _Grid = display.newGroup( )
	local cellSize
	local offset = 1

	--Define and set size of one cell
	cellSize = ((display.actualContentWidth) / col) - offset
	
	if ((cellSize * row) + (offset * (col + 1)) > display.actualContentHeight  - 150) then
		cellSize = ((display.actualContentHeight  - 150) / row) - offset
	end

	--Create cells
	for h = 1,row do
		for w = 1,col do
			_Grid:insert(
					widget.newButton(
					    {	
					    	shape = "Rect",
					    	width = cellSize,
					    	height = cellSize,
					        id = tostring( w .. "." .. h ),
					        onEvent = gridsListener,
					        fillColor = { default={1}, over={0.95} }
					    }
					)
				)
			_Grid[_Grid.numChildren]:translate( (w * (cellSize+offset)) - cellSize, (h * (cellSize + offset)) - cellSize )
			_Grid[_Grid.numChildren].anchorX = 0
			_Grid[_Grid.numChildren].anchorY = 0
		end
	end

	--_Grid:translate( display.contentCenterX, display.contentCenterY+50 )
	_Grid.y = display.contentCenterY + 50
	_Grid.x = display.contentCenterX
	_Grid.anchorChildren = true


		--Create lines between cells
	local backOfGrid = display.newRect(display.contentCenterX, display.contentCenterY+50,_Grid.width + 2*offset,_Grid.height+ 2*offset)
	scene.view:insert( backOfGrid )
	backOfGrid:setFillColor( 0.7,0.7,0.7)

	return _Grid
end

local function createTable( row,col )
	local _temp = {}
	for r=1,row do
		_temp[r] = {}
		for c=1,col do
			_temp[r][c] = 0
		end
	end
	return _temp
end

local function createTableScore( numPlayer )
	local table = {}
	for i=1,numPlayer do
		table[i] = 0 
	end
	return table
end

function newRound()
	local function clearGrid( )
		for i=1,grid.numChildren - row*col do
			grid[grid.numChildren]:removeSelf( )
			grid[grid.numChildren] = nil
		end

		for i=1, row*col do
			grid[i]:setEnabled( true )
		end

		moved = createTable(row,col)
	
		currentPlayer = math.random( 1, numPlayer )
		nextPlayer()
	end
	for i=row*col+1, grid.numChildren   do
		transition.fadeOut(grid[i], {time = 150, onComplete = function (  )
			clearGrid()
		end} )
	end


	
	

	setScoreText()
	currentPlayerView()
end



function scene:create( event )
    local sceneGroup = self.view
    row = event.params.rowCount
    col = event.params.colCount
    AIon = event.params.AIon
    numPlayer = event.params.playerCount
    lengthToWin = event.params.winCount
    moved = createTable(row,col)
    currentPlayer = math.random( 1, numPlayer )
    tableScore = createTableScore(numPlayer)

    interface = initInterface()
    sceneGroup:insert( interface )


    grid = initGrid(row,col)
    sceneGroup:insert( grid )

    initEndMenu()
    sceneGroup:insert( endMenuGroup )
   	
   	newRound()
    print( "Game scene have been created" )
end


-- "scene:show()"
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase


    if ( phase == "will" ) then
    	
    elseif ( phase == "did" ) then
        

    end
end


-- "scene:hide()"
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Called when the scene is on screen (but is about to go off screen).
        -- Insert code here to "pause" the scene.
        -- Example: stop timers, stop animation, stop audio, etc.
    elseif ( phase == "did" ) then
        -- Called immediately after scene goes off screen.
    end
end


-- "scene:destroy()"
function scene:destroy( event )

    local sceneGroup = self.view


end




-- -------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-- -------------------------------------------------------------------------------

return scene