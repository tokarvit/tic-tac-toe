application =
{

	content =
	{
		width = 730,
		height = 800, 
		scale = "zoomEven",
		fps = 60,
		
		
		imageSuffix =
		{
			    ["@2x"] = 1.5,
			    ["@4x"] = 3,
		},
		
	},

	--[[
	-- Push notifications
	notification =
	{
		iphone =
		{
			types =
			{
				"badge", "sound", "alert", "newsstand"
			}
		}
	},
	--]]    
}
